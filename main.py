import praw, time, datetime
import config as cfg
reddit = praw.Reddit(client_id=cfg.client_id,
                     client_secret=cfg.client_secret,
                     user_agent=cfg.user_agent,
                     username=cfg.username,
                     password=cfg.password)
    
subreddit = reddit.subreddit(cfg.sub)
session_authors={}
i=0
for comment in subreddit.stream.comments():
    author = comment.author
    age = int((int(time.time())-comment.author.created_utc)/(60*60*24))
    thread = comment.submission
    thread_title = comment.submission.title[0:35]
    if author in session_authors:
        session_authors[author]+=1
    else:
        session_authors[author]=1
    i=i+1
    now = datetime.datetime.utcfromtimestamp(comment.created_utc+(60*60*2)).strftime('%Y-%m-%d %H:%M:%SB')
    print(f"Comment {now}: {thread}-{comment}, Author: {str(author):>20}, {session_authors[author]:>5} of {i:>5} comments, Age[days]: {str(age):>5}, Title[0:35]: {thread_title}")
    
